package com.aeroclub.botwhatsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotWhatsappApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotWhatsappApplication.class, args);
	}

}
